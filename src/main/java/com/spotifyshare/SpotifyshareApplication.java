package com.spotifyshare;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan
public class SpotifyshareApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpotifyshareApplication.class, args);
	}
}



