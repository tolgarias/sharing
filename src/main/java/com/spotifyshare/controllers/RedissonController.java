package com.spotifyshare.controllers;

import com.spotifyshare.pojos.SpotifyList;
import com.spotifyshare.pojos.Tag;
import org.redisson.Config;
import org.redisson.Redisson;
import org.redisson.core.RList;
import org.redisson.core.RMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by bilal on 20/04/16.
 */

public class RedissonController {
    private Config cfg;
    private Redisson redisson;
    private static  String SPOTIFY_LISTS_HOLDER_NAME = "spotify_lists";
    private static  String TAG_LIST =  "list_tags";


    public RedissonController(){
            cfg = new Config();
            cfg.useSingleServer().setAddress("127.0.0.1:6379").setConnectionPoolSize(20);
            redisson = Redisson.create(cfg);
    }
    public RedissonController(String listHolder,String tagHolder){
        cfg = new Config();
        cfg.useSingleServer().setAddress("127.0.0.1:6379").setConnectionPoolSize(20);
        redisson = Redisson.create(cfg);
        SPOTIFY_LISTS_HOLDER_NAME = listHolder;
        TAG_LIST = tagHolder;
    }
    //For testing only
    public synchronized  void clearSpotifyLists(){
        RMap<String,SpotifyList> listMap = redisson.getMap(SPOTIFY_LISTS_HOLDER_NAME);
        listMap.clear();
    }
    public synchronized  String addList(SpotifyList list){
        String id = createId(list.getUid()+list.getSpotifyUri());
        list.setId(id);
        RMap<String,SpotifyList> listMap = redisson.getMap(SPOTIFY_LISTS_HOLDER_NAME);
        listMap.put(id,list);
        for(Tag tag:list.getTags()){
            addTag(tag);
        }
        return id;
    }

    public synchronized  SpotifyList getListByKey(String key){
        RMap<String,SpotifyList> listMap = redisson.getMap(SPOTIFY_LISTS_HOLDER_NAME);
        return listMap.get(key);
    }

    public synchronized  Collection<SpotifyList> getAllLists(){
        RMap<String,SpotifyList> listMap = redisson.getMap(SPOTIFY_LISTS_HOLDER_NAME);
        return listMap.values();
    }

    public List<SpotifyList> getListsByTag(Tag tag){
        return getAllLists().stream().filter((l)->l.getTags().contains(tag)).collect(Collectors.toList());
    }
    public List<SpotifyList> getListsByUser(String uid){
        return getAllLists().stream().filter((l)->l.getUid().equals(uid)).collect(Collectors.toList());
    }
    public List<SpotifyList> searchLists(String searchText){
        return getAllLists().stream().filter((l)->((l.getName()!=null && l.getName().toLowerCase().contains(searchText))||(l.getUsername()!=null && l.getUsername().toLowerCase().contains(searchText)))).collect(Collectors.toList());
    }

    public List<SpotifyList> getListsOrderByDate(){
        List<SpotifyList> lists = new ArrayList<>(getAllLists());
        lists.sort((l1,l2) -> compareListsByDate(l1,l2));
        return lists;
    }

    public List<SpotifyList> getListsOrderByRating(){
        List<SpotifyList> lists = new ArrayList<>(getAllLists());
        lists.sort((l1,l2) -> compareListsByRating(l1,l2));
        return lists;
    }
    public void removeListByKey(String key){
        RMap<String,SpotifyList> listMap = redisson.getMap(SPOTIFY_LISTS_HOLDER_NAME);
        listMap.remove(key);
    }
    public void  updateList(SpotifyList list){
        RMap<String,SpotifyList> listMap = redisson.getMap(SPOTIFY_LISTS_HOLDER_NAME);
        listMap.put(list.getId(),list);
    }



    private int compareListsByDate(SpotifyList l1,SpotifyList l2){
        return l1.getCreateDate()>l2.getCreateDate()?-1:(l1.getCreateDate()==l2.getCreateDate()?0:1);
    }
    private int compareListsByRating(SpotifyList l1,SpotifyList l2){
        return l1.getRating()>l2.getRating()?-1:(l1.getRating()==l2.getRating()?0:1);
    }



    private String createId(String code){
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(code.getBytes());
            byte[] hash = digest.digest();
            BigInteger bigInt = new BigInteger(1, hash);
            return bigInt.toString(16);
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
        return  "";
    }

    public void addTag(Tag tag){
        RList<Tag> tagList = redisson.getList(TAG_LIST);
        if(!tagList.contains(tag)){
            tagList.add(tag);
        }
    }

    public Collection<Tag> getTags(String query){
        RList<Tag> tags = redisson.getList(TAG_LIST);
        if(query==null || query.isEmpty()) {
            return tags;
        }
        else {
            return tags.stream().filter((l)->l.getText().contains(query)).collect(Collectors.toList());
        }
    }
    //for testing only
    public void clearTags(){
        RList<Tag> tags = redisson.getList(TAG_LIST);
        tags.clear();
    }




}
