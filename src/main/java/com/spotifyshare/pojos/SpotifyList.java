package com.spotifyshare.pojos;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bilal on 20/04/16.
 */
public class SpotifyList {
    private String spotifyUri;
    private String name;
    private String uid;
    private String username;
    private float rating;
    private int rateCount;
    private int totalRate;
    private List<Tag> tags;
    private List<Comment> comments;
    private String id;
    private Long createDate;
    private List<String> ratedUsers;

    public Long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Long createDate) {
        this.createDate = createDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public String getSpotifyUri() {
        return spotifyUri;
    }

    public void setSpotifyUri(String spotifyUri) {
        this.spotifyUri = spotifyUri;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public float getRating() {
        return rating;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public int getRateCount() {
        return rateCount;
    }

    public void setRateCount(int rateCount) {
        this.rateCount = rateCount;
    }

    public int getTotalRate() {
        return totalRate;
    }

    public void setTotalRate(int totalRate) {
        this.totalRate = totalRate;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public  SpotifyList(){
        tags = new ArrayList<>();
        comments = new ArrayList<>();
    }

    public void rateList(int value,String uid){
        rateCount++;
        totalRate+=value;
        rating=totalRate/rateCount;
        if(ratedUsers==null){
            ratedUsers = new ArrayList<>();
        }
        ratedUsers.add(uid);
    }
    public boolean isRated(String uid){
        boolean result = false;
        if(ratedUsers!=null){
            result = ratedUsers.contains(uid);
        }
        return result;
    }


}
