package com.spotifyshare.pojos;

/**
 * Created by bilal on 20/04/16.
 */
public class Tag {
    private String text;

    public String getText() {
        return text;
    }

    public void setText(String name) {
        this.text = name;
    }

    public  Tag(String name){
        this.text = name;
    }

    public  Tag(){

    }
    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Tag){
            return  this.text.equals(((Tag) obj).getText());
        }
        return  false;
    }
}
