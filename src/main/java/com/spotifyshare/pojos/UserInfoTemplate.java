package com.spotifyshare.pojos;

/**
 * Created by bilal on 30/06/16.
 */
public class UserInfoTemplate {
        private String id;
        private String last_name;
        private String first_name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

}
