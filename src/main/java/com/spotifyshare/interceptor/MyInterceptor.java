package com.spotifyshare.interceptor;

import com.spotifyshare.pojos.UserInfoTemplate;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by bilal on 30/06/16.
 */
@Component
public class MyInterceptor extends HandlerInterceptorAdapter{
    @Resource
    private Environment environment;

    private static final String GRAPH_URL = "https://graph.facebook.com/me?fields=id,first_name,last_name&access_token={access_token}";

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        try {
            int serverMode = environment.getProperty("server.mode",Integer.class);
            request.setAttribute("server.mode",serverMode);
            String accessToken = request.getParameter("access_token");
            if (accessToken != null && !accessToken.isEmpty()) {
                request.setAttribute("userInfo", getUserInfoFromAccessToken(accessToken));
            }
            else if (serverMode==0){
                request.setAttribute("userInfo", getMockUserInfoTemplate());
            }
            String pageParameter = request.getParameter("p");
            int page = 1;
            if(pageParameter!=null && !pageParameter.isEmpty()){
                page = Integer.parseInt(pageParameter);
            }
            request.setAttribute("page",page);
        }
        catch (Exception ex){
            //TODO:log this
        }
        return super.preHandle(request, response, handler);
    }


    private UserInfoTemplate getUserInfoFromAccessToken(String accessToken){
        RestTemplate restTemplate = new RestTemplate();
        Map<String,String> vars = new HashMap<>();
        vars.put("access_token",accessToken);
        UserInfoTemplate result = restTemplate.getForObject(GRAPH_URL,UserInfoTemplate.class,vars);
        return result;

    }
    private UserInfoTemplate getMockUserInfoTemplate(){
        UserInfoTemplate mockUser = new UserInfoTemplate();
        mockUser.setId("test_uid");
        mockUser.setFirst_name("John");
        mockUser.setLast_name("Doe");
        return mockUser;
    }


}
