package com.spotifyshare;

import com.spotifyshare.controllers.RedissonController;
import com.spotifyshare.pojos.Comment;
import com.spotifyshare.pojos.SpotifyList;
import com.spotifyshare.pojos.Tag;
import com.spotifyshare.pojos.UserInfoTemplate;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.validator.constraints.ParameterScriptAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collection;
import java.util.List;

/**
 * Created by bilal on 20/04/16.
 */

@RestController
public class ShareListRestController {
    private static final int SIZE = 5;
    private static final Logger logger = LogManager.getLogger(ShareListRestController.class);

    @Resource
    private RedissonController redissonController;


    @RequestMapping(value = "/lists/all",method = RequestMethod.GET)
    public Collection<SpotifyList> getLists(){
        return redissonController.getAllLists();
    }

    @RequestMapping(value = "/lists/add",method = RequestMethod.POST)
    public void addList(@RequestBody SpotifyList list, HttpServletRequest request) {
        try {
            UserInfoTemplate userInfo = (UserInfoTemplate) request.getAttribute("userInfo");
            if (userInfo != null) {
                list.setUid(userInfo.getId());
                list.setCreateDate(System.currentTimeMillis());
                list.setUsername(userInfo.getFirst_name() + " " + userInfo.getLast_name().substring(0, 1) + ".");
            }
            redissonController.addList(list);
        }
        catch (Exception ex){
            logger.error("error when adding the list",ex);
        }

    }
    @RequestMapping("/list/{list_id}")
    public SpotifyList getListById(@PathVariable String list_id){
        SpotifyList result = null;
        try {
            result = redissonController.getListByKey(list_id);
        }
        catch (Exception ex){
            logger.error("error when getting the list",ex);
        }
        return result;
    }

    @RequestMapping("/lists/{tag}")
    public Collection<SpotifyList> getListsByTag(@PathVariable String tag, HttpServletRequest request, HttpServletResponse response){
        Collection<SpotifyList> result = null;
        try {
            Tag tagObject = new Tag(tag);
            List<SpotifyList> lists = redissonController.getListsByTag(tagObject);
            result = paginateListRequest(lists, request, response);
        }
        catch (Exception ex){
            logger.error("error when getting lists by tag",ex);
        }

        return result;
    }

    @RequestMapping("lists/all/bydate")
    public Collection<SpotifyList> getListsByDate(HttpServletRequest request,HttpServletResponse response){
        Collection<SpotifyList> result = null;
        try {
            result = paginateListRequest(redissonController.getListsOrderByDate(), request, response);
        }catch (Exception ex){
            logger.error("error when getting lists ordered by date",ex);
        }
        return result;
    }

    @RequestMapping("lists/all/byrating")
    public Collection<SpotifyList> getListsByRating(HttpServletRequest request,HttpServletResponse response){
        Collection<SpotifyList> result = null;
        try {
            result = paginateListRequest(redissonController.getListsOrderByRating(),request,response);
        }catch (Exception ex){
            logger.error("error when getting lists ordered by rating",ex);
        }
        return result;
    }

    @RequestMapping("lists/byuser/{user_id}")
    public Collection<SpotifyList> getListsByUser(@PathVariable String user_id,HttpServletRequest request,HttpServletResponse response){
        Collection<SpotifyList> result = null;
        try {
            result = paginateListRequest(redissonController.getListsByUser(user_id),request,response);
        }catch (Exception ex){
            logger.error("error when getting lists by user",ex);
        }
        return result;
    }

    @RequestMapping(value = "/list/update",method = RequestMethod.POST)
    public SpotifyList updateList(@RequestBody SpotifyList list,HttpServletRequest request){
        try {
            UserInfoTemplate userInfo = (UserInfoTemplate) request.getAttribute("userInfo");
            SpotifyList listFromMemory = redissonController.getListByKey(list.getId());
            if (verifyUser(userInfo, listFromMemory)) {
                redissonController.updateList(list);
            }
        }catch (Exception ex){
            logger.error("error when updating the list",ex);
        }
        return redissonController.getListByKey(list.getId());
    }

    @RequestMapping(value="/list/comments/add/{list_id}",method = RequestMethod.POST)
    public SpotifyList addCommentToList(@PathVariable String list_id, @RequestBody Comment comment,HttpServletRequest request){
        UserInfoTemplate userInfo = (UserInfoTemplate) request.getAttribute("userInfo");
        SpotifyList list = redissonController.getListByKey(list_id);
        if(userInfo!=null) {
            try {
                comment.setUid(userInfo.getId());
                comment.setName(userInfo.getFirst_name() + " " + userInfo.getLast_name().substring(0, 1) + ".");
                list.getComments().add(comment);
                redissonController.updateList(list);
            }
            catch (Exception ex){
                logger.error("error when adding comment to the list",ex);
            }
        }
        return list;
    }

    @RequestMapping("/list/rate/{list_id}/{rating}")
    public ResponseEntity<SpotifyList> rateList(@PathVariable String list_id, @PathVariable int rating, HttpServletRequest request){
        UserInfoTemplate userInfo = (UserInfoTemplate) request.getAttribute("userInfo");
        SpotifyList list = redissonController.getListByKey(list_id);
        HttpStatus status = HttpStatus.OK;
        if(userInfo!=null) {
            try {
                if (!list.isRated(userInfo.getId())) {
                    list.rateList(rating, userInfo.getId());
                    redissonController.updateList(list);
                } else {
                    status = HttpStatus.NOT_ACCEPTABLE;
                }
            }catch (Exception ex){
                logger.error("error when rating the list",ex);
            }
        }
        return new ResponseEntity<SpotifyList>(list,null,status);

    }

    @RequestMapping("/list/delete/{list_id}")
    public void deleteList(@PathVariable String list_id,HttpServletRequest request){
        UserInfoTemplate userInfo = (UserInfoTemplate) request.getAttribute("userInfo");
        SpotifyList list = redissonController.getListByKey(list_id);
        try {
            if (verifyUser(userInfo, list)) {
                redissonController.removeListByKey(list.getId());
            }
        }
        catch (Exception ex){
            logger.error("error when deleting the list",ex);
        }
    }

    @RequestMapping("/tags")
    public Collection<Tag> getTags(@RequestParam(name="query",defaultValue = "") String query){
        Collection<Tag> result = null;
        try {
            result = redissonController.getTags(query);
        }
        catch (Exception ex){
            logger.error("error when getting tags",ex);
        }
        return result;
    }
    @RequestMapping(value = "/lists/search",method = RequestMethod.POST)
    private  Collection<SpotifyList> searchLists(@RequestBody String searchText){
        Collection<SpotifyList> result = null;
        try {
            searchText = searchText.toLowerCase();
            result = redissonController.searchLists(searchText);
        }
        catch (Exception ex){
            logger.error("error when searching lists",ex);
        }
        return result;
    }

    private boolean verifyUser(UserInfoTemplate userInfo,SpotifyList list){
        if(userInfo!=null && list.getUid().equals(userInfo.getId())){
            return true;
        }
        return false;
    }
    private Collection<SpotifyList> paginateListRequest(List<SpotifyList> lists,HttpServletRequest request,HttpServletResponse response){
        int page = (int)request.getAttribute("page");
        if(page<1) page = 1;

        int pageCount = lists.size()/SIZE;
        if(pageCount*SIZE<lists.size()) pageCount++;

        response.addHeader("Access-Control-Expose-Headers","page_count");
        response.addHeader("page_count",String.valueOf(pageCount));
        int end = page*SIZE;
        if(end>lists.size()) end = lists.size();
        return  lists.subList((page-1)*SIZE,end);
    }



}
