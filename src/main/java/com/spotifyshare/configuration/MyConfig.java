package com.spotifyshare.configuration;

import com.spotifyshare.controllers.RedissonController;
import com.spotifyshare.interceptor.MyInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.config.annotation.CorsRegistration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import javax.annotation.Resource;

/**
 * Created by bilal on 30/06/16.
 */
@Configuration
@PropertySource("classpath:application.properties")
public class MyConfig extends WebMvcConfigurerAdapter{
    @Autowired
    MyInterceptor interceptor;
    public  MyConfig(){

    }
    @Resource
    private Environment environment;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        super.addInterceptors(registry);
        registry.addInterceptor(interceptor);
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        super.addCorsMappings(registry);
        CorsRegistration registration = registry.addMapping("/**");
        String allowedOrigins = environment.getProperty("server.allowed_origins","");
        registration.allowedOrigins(allowedOrigins.split(","));
    }

    @Bean
    public RedissonController getRedissonController(){
            return new RedissonController(environment.getProperty("redis.list_holder_name"),environment.getProperty("redis.tag_holder_name"));

    }
}
