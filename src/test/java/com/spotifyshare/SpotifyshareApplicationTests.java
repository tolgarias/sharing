package com.spotifyshare;

import com.spotifyshare.controllers.RedissonController;
import com.spotifyshare.pojos.Comment;
import com.spotifyshare.pojos.SpotifyList;
import com.spotifyshare.pojos.Tag;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SpotifyshareApplication.class)

public class SpotifyshareApplicationTests {

	@Resource
	private RedissonController redissonController;

	private String addedListId;
	private SpotifyList mockList = null;
	@Before
	public  void init(){
		//TODO: connection with redis
		redissonController.clearSpotifyLists();

		mockList = new SpotifyList();
		mockList.setName("test");
		mockList.setUid("745780221");
		mockList.setCreateDate(System.currentTimeMillis());
		mockList.setSpotifyUri("t");
		mockList.setRating(5);
		Tag tag = new Tag();

		tag.setText("rock");
		mockList.getTags().add(tag);
		addedListId = redissonController.addList(mockList);


		SpotifyList mockList2 = new SpotifyList();
		mockList2.setName("test 2");
		mockList2.setUid("11111");
		mockList2.setSpotifyUri("uri");
		mockList2.getTags().add(new Tag("pop"));
		mockList2.setCreateDate(System.currentTimeMillis()-1000000);
		mockList2.setRating(4.2f);
		redissonController.addList(mockList2);




	}
	@Test
	public void clearLists(){
		redissonController.clearSpotifyLists();
	}

	@Test
	public void getListById(){
		//TODO: get added list
		SpotifyList list = redissonController.getListByKey(addedListId);
		assertEquals(list.getId(),addedListId);
	}

	@Test
	public void getListsByTag(){
		//TODO: get list by tag
		Collection<SpotifyList> listsByTag = redissonController.getListsByTag(new Tag("rock"));
		assertTrue(listsByTag.size()>0);
	}

	@Test
	public void getListsOrderByCreateDate(){
		List<SpotifyList> listsOrderrByCreateDate = redissonController.getListsOrderByDate();
		assertEquals(listsOrderrByCreateDate.get(0).getId(),addedListId);
	}

	@Test
	public void getListsByUser(){
		assertTrue(redissonController.getListsByUser("745780221").size()>0);
	}

	@Test
	public void  getListsOrderByRating(){
		List<SpotifyList> listsOrderrByRating = redissonController.getListsOrderByRating();
		assertEquals(listsOrderrByRating.get(0).getId(),addedListId);
	}


	@Test
	public void updateList(){
		SpotifyList list = redissonController.getListByKey(addedListId);
		list.setName("new name");
		redissonController.updateList(list);
		SpotifyList list2 = redissonController.getListByKey(addedListId);
		assertEquals(list.getName(),list2.getName());
	}


	@Test
	public void deleteList(){
		redissonController.removeListByKey(addedListId);
		SpotifyList list = redissonController.getListByKey(addedListId);
		assertEquals(list,null);
	}


	@Test
	public void  addCommentToList(){
		List<SpotifyList> listsByUser =redissonController.getListsByUser("11111");
		SpotifyList list = listsByUser.get(0);
		list.getComments().add(new Comment());
		redissonController.updateList(list);

		SpotifyList list2 = redissonController.getListByKey(list.getId());
		assertTrue(list.getComments().size()>0);

	}

	@Test
	public void addTagToTagList(){
		redissonController.clearTags();
		redissonController.addTag(new Tag("pop"));
	}







}
