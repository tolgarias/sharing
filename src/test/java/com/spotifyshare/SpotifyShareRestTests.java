package com.spotifyshare;

import com.spotifyshare.controllers.RedissonController;
import com.spotifyshare.pojos.Comment;
import com.spotifyshare.pojos.SpotifyList;
import com.spotifyshare.pojos.Tag;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.hamcrest.Matchers.hasItem;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Created by bilal on 24/06/16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(SpotifyshareApplication.class)
@WebAppConfiguration
public class SpotifyShareRestTests {

    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));
    private MockMvc mockMvc;
    private HttpMessageConverter mappingJackson2Http;
    private final String uid = "test_uid";

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private  RedissonController redissonController;

    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters){
        this.mappingJackson2Http = Arrays.asList(converters).stream().filter(hmc->hmc instanceof MappingJackson2HttpMessageConverter).findAny().get();
        Assert.assertNotNull("the json converters must not be null",this.mappingJackson2Http);
    }

    @Before
    public void setup() throws Exception {
        redissonController.clearSpotifyLists();
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void createList() throws Exception{
        SpotifyList mockList2 = new SpotifyList();
        mockList2.setName("test 2");
        mockList2.setUid(uid);
        mockList2.setSpotifyUri("uri");
        mockList2.getTags().add(new Tag("pop"));
        mockList2.setCreateDate(System.currentTimeMillis()-1000000);
        mockList2.setRating(4.2f);

        mockMvc.perform(post(makeUrl("/lists/add")).content(json(mockList2)).contentType(contentType)).andExpect(status().isOk());

    }
    @Test
    public void getAllLists() throws Exception{
        redissonController.clearSpotifyLists();
        addMockList();
        mockMvc.perform(get("/lists/all").contentType(contentType)).andExpect(status().isOk()).andExpect(jsonPath("$",hasSize(1)));
    }
    @Test
    public void getListById() throws Exception{
        //TODO: get added list
        SpotifyList mockList = addMockList();
        String url = "/list/"+mockList.getId();
        mockMvc.perform(get(url).contentType(contentType)).andExpect(status().isOk()).andExpect(jsonPath("$.id",is(mockList.getId())));

    }

    @Test
    public void getListsByTag() throws Exception{
        //TODO: get list by tag
        redissonController.clearSpotifyLists();
        addMockList();
        String tag = "pop";
        String url = "/lists/"+tag;
        mockMvc.perform(get(url).contentType(contentType)).andExpect(status().isOk()).andExpect(jsonPath("$[0].tags[0].text",is(tag)));

    }

    @Test
    public void getListsOrderByCreateDate() throws Exception{
        redissonController.clearSpotifyLists();
        addMockList();
        String url = "/lists/all/bydate";
        mockMvc.perform(get(url).contentType(contentType)).andExpect(status().isOk()).andExpect(jsonPath("$",hasSize(1)));
    }

    @Test
    public void getListsByUser() throws Exception{
        redissonController.clearSpotifyLists();
        addMockList();
        String url = "/lists/byuser/"+uid;
        mockMvc.perform(get(url).contentType(contentType)).andExpect(status().isOk()).andExpect(jsonPath("$[0].uid",is(uid)));
    }

    @Test
    public void  getListsOrderByRating() throws Exception{
        redissonController.clearSpotifyLists();
        addMockList();
        String url = "/lists/all/byrating";
        mockMvc.perform(get(url).contentType(contentType)).andExpect(status().isOk()).andExpect(jsonPath("$",hasSize(1)));
    }


    @Test
    public void updateList() throws Exception{
        SpotifyList list = addMockList();
        String newName = "new name";
        list.setName(newName);
        String url = makeUrl("/list/update");
        mockMvc.perform(post(url).contentType(contentType).content(json(list))).andExpect(status().isOk()).andExpect(jsonPath("$.name",is(newName)));
    }





    @Test
    public void  addCommentToList() throws Exception{
        redissonController.clearSpotifyLists();
        SpotifyList list = addMockList();
        Comment comment = new Comment();
        comment.setName("2222");
        comment.setComment("11111111");
        String url=makeUrl("/list/comments/add/"+list.getId());
        mockMvc.perform(post(url).content(json(comment)).contentType(contentType)).andExpect(status().isOk()).andExpect(jsonPath("$.comments",hasSize(1)));
    };

    @Test
    public void  rateList() throws Exception{
        SpotifyList list = addMockList();
        int rating = 5;
        String url=makeUrl("/list/rate/"+list.getId()+"/"+rating);
        mockMvc.perform(get(url).contentType(contentType)).andExpect(status().isOk());
    };

    @Test
    public void deleteList() throws Exception{
        redissonController.clearSpotifyLists();
        SpotifyList list = addMockList();
        String url = makeUrl("/list/delete/"+list.getId());
        mockMvc.perform(get(url).contentType(contentType)).andExpect(status().isOk());
    }

    @Test
    public void searchLists() throws Exception{
        String searchText = "test";
        redissonController.clearSpotifyLists();
        SpotifyList list = addMockList();
        String url = makeUrl("/lists/search/");
        mockMvc.perform(post(url).contentType(contentType).content(searchText)).andExpect(status().isOk()).andExpect(jsonPath("$",hasSize(1)));
    }

    @After
    public void after(){
        redissonController.clearSpotifyLists();
    }


    protected String json(Object o) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        this.mappingJackson2Http.write(
                o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }

    protected String makeUrl(String url){
        return url;
    }
    protected SpotifyList addMockList(){
        SpotifyList mockList = new SpotifyList();
        mockList.setName("test 2");
        mockList.setUid(uid);
        mockList.setSpotifyUri("uri");
        mockList.getTags().add(new Tag("pop"));
        mockList.setCreateDate(System.currentTimeMillis()-1000000);
        mockList.setRating(4.2f);
        String id = redissonController.addList(mockList);
        mockList.setId(id);
        return mockList;
    }

}
